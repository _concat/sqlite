[![Author](http://img.shields.io/badge/author-@rudi_theunissen-blue.svg?style=flat)](https://twitter.com/rudi_theunissen)
[![Build Status](https://img.shields.io/travis/concat/sqlite/master.svg?style=flat)](https://travis-ci.org/concat/sqlite)
[![Software License](https://img.shields.io/packagist/l/concat/sqlite.svg?style=flat)](LICENSE.md)
[![Latest Version](https://img.shields.io/github/tag/concat/sqlite.svg?style=flat&label=release)](https://github.com/concat/sqlite/tags)
[![Total Downloads](https://img.shields.io/packagist/dt/concat/sqlite.svg?style=flat)](https://packagist.org/packages/concat/sqlite)
[![Version](https://img.shields.io/packagist/v/concat/sqlite.svg?style=flat)](https://packagist.org/packages/concat/sqlite)
[![Test Coverage](https://codeclimate.com/github/concat/sqlite/badges/coverage.svg?style=flat)](https://codeclimate.com/github/concat/sqlite)
[![Code Climate](https://codeclimate.com/github/concat/sqlite/badges/gpa.svg?style=flat)](https://codeclimate.com/github/concat/sqlite)

This package is compliant with [PSR-1], [PSR-2] and [PSR-4]. If you notice compliance oversights,
please send a patch via pull request.

[PSR-1]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
[PSR-2]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md

## Install

Via Composer

``` json
require: {
    "concat/sqlite": "dev-master"
}
```
